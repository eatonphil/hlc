# HLC

HLC is a Lisp-like syntax on top of C implemented as Common Lisp macros.

For instance, this HLC example:

```
(include "stdio.h")

(def main ((int argc) (char** argv)) int
     (printf "Hello, %s! Today is the %dst.\\n" "Phil" 21)
     (return 0))
```

When hooked up to HLC's macros produces:

```
#include <stdio.h>

int main(int argc, char** argv) {
 printf("Hello, %s! Today is the %dst.\n", "Phil", 21);
 return 0;
}
```