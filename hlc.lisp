(defmacro include (&rest names)
  (loop for name in names
      do (format t "#include <~(~a~)>~%" name))
  (format t "~%"))

(defmacro expression->string (expression)
  `(cond
     ((stringp ,expression) (format nil "\"~a\"" ,expression))
     ((integerp ,expression) (format nil "~a" ,expression))))

(defmacro expression-or-statement->string (expression-or-statement)
  `(destructuring-bind (first &rest rest) ,expression-or-statement
     (cond
       ;; If is statement
       ((member first '(return goto)) (format nil "~T~(~a~) ~a" first (car rest)))
       ;; Else if is expression
       (t (format nil "~(~a~)(~{~a~^, ~})"
                  first
                  (loop for expression in rest
                     collect (eval (expression->string expression))))))))

(defmacro def (name in-args out-arg &rest body)
  (let ((formatted-in-args
         (format nil "~{~{~(~a~)~^ ~}~^, ~}" in-args))
        (formatted-body
         (loop for expression-or-statement in body
              collect (eval (expression-or-statement->string expression-or-statement)))))
    (format t "~(~a~) ~(~a~)(~a) {~%~T~{~a;~%~}}~%" out-arg name formatted-in-args formatted-body)))

(include "stdio.h")

(def main ((int argc) (char** argv)) int
     (printf "Hello, %s! Today is the %dst.\\n" "Phil" 21)
     (return 0))
